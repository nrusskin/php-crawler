<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class Matask
{
	public function __construct(\Slim\Slim $app, $path)
	{
		$app->group($path, function () use ($app) {
			$app->get('/page/:page', function ($page) use ($app) {
				$result = [];
				$multipart = [];
				$client = new \Goutte\Client();
				$document = $client->request('POST', 'https://www.matas.dk/hkb/Product/_ListAjax', [
					'ProductGroupId'	=> 557534,
					'Page'				=> $page,
					'ListBaseType'		=> 'Product',
					'ProductList.order'	=> 0,
					'Order'				=> 0
				]);

				$document->filter('li.productItem')->each(function($product) use (&$result) {
					$item = [];
					$this->getValue($product, '.product-heading-row', $item, 'title', function($title) {return $title->text();});

					$this->getValue($product, '.btn-price', $item, 'price', function ($price) {return trim($price->text(), " \r\n.")/100;});

					$this->getValue($product, 'img', $item, 'img', function($title) {return $title->attr('src');});

					$this->getValue($product, 'a.url', $item, 'url', function($title) {return $title->attr('href');});
					$result[] = $item;
				});

				echo json_encode($result);
			});
		});
	}

	protected function getValue($node, $filter, array &$item, $name, $process_cb)
	{
		$val = $node->filter($filter);
		if (count($val))
				$item[$name] = $process_cb($val);
	}
}
