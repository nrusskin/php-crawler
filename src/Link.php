<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class Link
{
	public function __construct(\Slim\Slim $app, $path)
	{
		$app->group($path, function () use ($app) {
			$app->get('/', function () use ($app) {
				$url = $app->request->get('url');
				if (!$url)
					$app->halt(404);

				$result = [];
				$client = new \Goutte\Client();
				$document = $client->request('GET', $url);

				$document->filter('a')->each(function($info) use (&$result, $url) {
					$link = $info->attr('href');
					if ( !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $link))
						$link = $url . $link;
					$result[] = ['text' => $info->text(), 'link' => $link];
				});

				echo json_encode($result);
			});
		});
	}
}
