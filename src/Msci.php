<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class Msci
{
	public function __construct(\Slim\Slim $app, $path)
	{
		$app->group($path, function () use ($app) {
			$app->get('/chapt/:name', function ($name) use ($app) {
				$result = [];
				$url = 'http://www2.msci.org/apps/chapters/chapterofficers.aspx?CHAPT=' . $name;
				$client = new \Goutte\Client();
				$document = $client->request('GET', $url);

				$document->filter('td a')->each(function($info) use (&$result, $url) {
					$link = $info->attr('href');
					$link = str_replace('mailto:', '', $link);
					$result[] = ['name' => $info->text(), 'email' => $link];
				});

				echo json_encode($result);
			});
		});
	}
}
