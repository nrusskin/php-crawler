<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class Amazon
{
	public function __construct(\Slim\Slim $app, $path)
	{
		$app->group($path, function () use ($app) {
			$app->get('/bestsellers/categories', function () use ($app) {
				$result = [];
				$client = new Filter();
				$client->get('http://www.amazon.com/Best-Sellers/zgbs',
					'#zg_browseRoot > ul > li > a',
					function ($node) use (&$result) {
						$result[] = [
							'url' => $node->attr('href'),
							'name'=> $node->text()];
				});
				echo json_encode($result);
			});

			$app->get('/bestsellers/category/', function () use ($app) {
				if (!isset($_GET['url']))
					$app->halt(400, 'Missing parameter "url"');

				$result = [];
				$client = new Filter();
				$client->get($_GET['url'],
					'.zg_itemWrapper',
					function ($node) use (&$result) {

						$title = $node->filter('.zg_title > a');
						$price = $node->filter('.price');
						$item = [
							'title'	=> $title->text(),
							'url'	=> ltrim($title->attr('href'), "\n."),
							'price'	=> count($price) ? $price->text() : ''];

						$rateNode = $node->filter('.a-icon-alt');
						if (count($rateNode)) {
							$rate = [];
							preg_match('/([0-9]\.[0-9])/', $rateNode->text(), $rate);
							$item['rate']	= $rate[0];
						}
						$result[] = $item;
				});
				echo json_encode($result);
			});

			$app->get('/bestsellers/product/', function () use ($app) {
				if (!isset($_GET['url']))
					$app->halt(400, 'Missing parameter "url"');

				$client = new Filter();
				$document = $client->get($_GET['url']);

				$title = $document->filter('#productTitle');
				if (!count($title))
					$title = $document->filter('#btAsinTitle');
				if (count($title))
					$result = [ 'title'	=> $title->text()];

				$document->filter('#mbc > .a-box span.a-size-small > a')->each(function($sellers) use (&$result) { 
					$result['sellers']	= $sellers->text();
					$result['url']		= 'http://www.amazon.com' . $sellers->attr('href');
				});

				echo json_encode($result);
			});
		});
	}
}
