<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class Filter
{
	/**
	 * @var \Goutte\Client
	 */
	private $client;

	public function __construct() {
		$this->client = new \Goutte\Client();
	}

	public function setHeader($name, $value){
		$this->client->setHeader($name, $value);
		return $this;
	}

	public function get($url, $selector = null, $callback = null, $method='GET') {
		$document = $this->client->request($method, $url);

		if ($selector && $callback)
			$document->filter($selector)->each($callback);

		return $document;
	}
}
