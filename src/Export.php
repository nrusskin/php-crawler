<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class Export
{
	public function __construct(\Slim\Slim $app, $path)
	{
		$app->post($path, function () use ($app) {
			$this->setHeaders($app);
			$data = $app->request()->post('data');
			$data = json_decode($data, true);
			$output = fopen('php://output', 'w');
			foreach($data as $row)
				fputcsv($output, $row, ";");
			fclose($output);
			//print_r($data[0]);
			return;
			$outputData = json_decode($app->request->getBody(),true);
			$this->dump($outputData);
		});

	}

	private function setHeaders(\Slim\Slim $app)
	{
		$headers = $app->response->headers;
		$headers->set('Content-Type', 'text/csv; charset=utf-8');
		$headers->set('Content-Disposition', 'attachment; filename="export.csv"');
	}

	private function convertRequest($request)
	{
		return $request;

	}

	private function dump($result)
	{
		$output = fopen('php://output', 'w');

		$firstLineKeys = false;
		if (empty($firstLineKeys) && count($result))
		{
			$firstLineKeys = array_keys(json_decode($result[0], true));
			fputcsv($output, $firstLineKeys);
			$firstLineKeys = array_flip($firstLineKeys);
		}

		foreach($result as $row)
			fputcsv($output, json_decode($row,true), ";");
		fclose($output);
	}
}
