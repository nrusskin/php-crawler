<?php

namespace PHPCrawler;

/**
 * @author Nikolay Russkin
 */
class TrovaBusinessDirectory
{
	public function __construct(\Slim\Slim $app, $path)
	{
		$app->group($path, function () use ($app) {
			$app->get('/page/:page', function ($page) use ($app) {
				$result = [];
				$multipart = [];
				$client = new \Goutte\Client();
				$document = $client->request('GET', 'http://www.trovabusinessdirectory.com/results.php?screen=' . $page);

				$document->filter('div.summaryComplementaryContent')->each(function($product) use (&$result) {
					$item = [];
					$links = $product->filter('p > a')->each(function ($info) use (&$item) {
						$prev = $info->previousAll();
						if (count($prev)) {
							if ($prev->text() == 'w: ') {
								if ( preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $info->text()))
									$item['site'] = $info->text();
								else
									$item['site'] = $info->attr('href');
							} else if ($prev->text() == 'e: ') {
								$item['email'] = $info->text();
							}
						}
					});

					if (isset($item['email']))
						$result[] = $item;
				});

				echo json_encode($result);
			});
		});
	}

	protected function getValue($node, $filter, array &$item, $name, $process_cb)
	{
		$val = $node->filter($filter);
		if (count($val))
				$item[$name] = $process_cb($val);
	}
}
