<?php

ini_set('max_execution_time', 9000);

require '../vendor/autoload.php';

$url = $_GET['q'];

echo '<form><input type="text" placeholder="url" value="https://www.residentadvisor.net/events.aspx?ai=13&v=day&mn=4&yr=2016&dy=2" name="q"/><button type="submit">Start</button></form>';

if (!$url) {
	return;
}

$loader = new \PHPCrawler\Filter();

$loader->get($url, '.event-item > a', function ($node, $i) use($loader) {
		$server = 'https://www.residentadvisor.net';
		$url = $server . $node->attr('href');

		$event = $loader->get($url);
		
		$title = $event->filter('#sectionHead > h1')->first()->text();

		$info = $event->filter('.clearfix > li')->first();

		$date = $info->filter('a')->text();

		$html = $info->html();
		preg_match('/([^\>]+)$/', $html, $time);

		$info = $event->filter('.clearfix > .wide')->first();
		$html = $info->html();
		preg_match('/\<br\>([^\>]+)\<br\>/', $html, $address);

		$info = $event->filter('#event-item > div > p')->eq(1);
		$description = $info->text();

		$info = $event->filter('#event-item > div > a > img');
		if ($info->count() > 0)
			$img = $info->attr('src');

		print "\n-----" . $i . $date . ';' .
			$time[0] . ';' . 
			$address[0] . ';' . 
			$title . ';' . 
			$description. ';' . 
			$server . $img. ';' . 
			$server . $node->attr('href') . "\n";

		exit;
});

