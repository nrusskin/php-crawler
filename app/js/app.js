'use strict';

angular.module('crawler', [
	'ngResource',
	'ui.router',
	'module.Export',
	'crawler.Examples',
		'crawler.Amazon',
		'crawler.Alibaba',
		'crawler.Angel',
		'crawler.Lamudi',
		'crawler.Link', 
		'crawler.Matask', 
		'crawler.Msci',
		'crawler.Privateproperty',
		'crawler.Residentadvisor',
		'crawler.Trova', 
])
.config(['$urlRouterProvider', '$locationProvider', '$httpProvider', function ($urlRouterProvider, $locationProvider, $httpProvider) {
	//enable HTML5 routing mode
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});

	// cofigure ui.router
	$urlRouterProvider.otherwise('/app');

	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);
