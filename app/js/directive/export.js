angular.module('module.Export', [])
.directive('postNewWindow', ['$window', '$compile', function($window, $compile) {
	var exportFn = function(path, params, method) {
		var form = document.createElement("form");
		form.setAttribute("method", method || "POST");
		form.setAttribute("action", path);
		form.setAttribute("target", "_blank");

		for(var key in params) {
			if(params.hasOwnProperty(key)) {
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", key);
				hiddenField.setAttribute("value", JSON.stringify(params[key]));

				form.appendChild(hiddenField);
			 }
		}

		document.body.appendChild(form);
		form.submit();
		document.body.removeChild(form);
	};

	return {
		restrict: 'EA',
		scope: {},
		template:'',
		link: function($scope, $element, attr) {
			$scope.$on('exportStart',function(event, info){
				exportFn(info.url, {data : info.data});
			});
		}
	}
}]);
