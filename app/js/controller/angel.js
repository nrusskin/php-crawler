'use strict';

angular.module('crawler.Angel', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('angel', {
		parent : 'home',
		url: '/app/angel/',
		views: {
			'content@': {
				templateUrl: '/app/partials/angel.html',
				controller: 'AngelCtrl'
			}
		}
	});
}])

.controller('AngelCtrl', ['$scope', '$resource', function ($scope, $resource) {
	$scope.startups = [];

	$resource('/api/v1/angel/startup/search/:city', {city:'Singapore'}, {get : {method : 'GET'}})
	.get( function (data) {
		delete data.$promise;
		delete data.$resolved;
		data['ids[]'] = data.ids;
		delete data.ids;
		$resource('/api/v1/angel/startup', data, {query : {method : 'GET', isArray : true}})
		.query( function(urls){
			var startupName = 'haven';
			urls.forEach( function (startupName) {
				$resource('/api/v1/angel/startup/:name', {name : startupName}, {get : {method : 'GET'}})
				.get( function(info) {
					$scope.startups.push(info);
				});
			});
		});
	});
}]);

