'use strict';

angular.module('crawler.Alibaba', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('alibaba', {
		parent : 'home',
		url: '/app/alibaba',
		views: {
			'content@': {
				templateUrl : '/app/partials/alibaba.html',
				controller : 'AlibabaCtrl'
			}
		}
	});

}])

.controller('AlibabaCtrl', ['$scope', '$http', '$resource', '$state', function ($scope, $http, $resource, $state) {
	// Export callback
	$scope.export = function() {
		// titles
		var data = [{
			name : 'name',
			minPrice : 'min price',
			maxPrice : 'max price',
			currency : 'currency',
			buyers : 'buyers',
			url : 'url'
		}];
		// transform data to array
		$scope.categories.forEach(function (product) {
			data.push({
				name : product.name,
				minPrice : product.minPrice,
				maxPrice : product.maxPrice,
				currency : product.fobPriceUnit,
				buyers : product.buyerCount,
				url : 'https:' + product.url
			});
		});
		// send "export" notification
		$scope.$broadcast('exportStart', {
			url : '/api/v1/export',
			data : data
		});
	};

	// load data
	$scope.load = function () {
		$scope.categories = [];

		for (var i = 1; i < 11; ++i) {
			var params = {
				'page' : i,
				'callback': 'JSON_CALLBACK'
			};
			var url = 'https://rfq.alibaba.com/rfq/searchListAjax.do?pageSize=36';

			$http({
				url: url,
				method: 'JSONP',
				params: params
			}).success(function (data) {
				$scope.categories = $scope.categories.concat(data.productList);
			});
		}
	};
}]);

