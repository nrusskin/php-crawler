'use strict';

angular.module('crawler.Amazon', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('amazon', {
		parent : 'home',
		url: '/app/amazon',
		views: {
			'content@': {
				templateUrl : '/app/partials/amazon.html',
				controller : 'AmazonCtrl'
			}
		}
	});

}])

.controller('AmazonCtrl', ['$scope', '$resource', '$state', function ($scope, $resource, $state) {
	// Export callback
	$scope.export = function() {
		// titles
		var data = [{
			title : 'title',
			category : 'category',
			price : 'price',
			url : 'url'
		}];
		// transform data to array
		$scope.categories.forEach(function (cat) {
			cat.products.forEach(function (product) {
				data.push({
					title : product.title,
					category : cat.name,
					price : product.price,
					url : product.url
				});
			});
		});
		// send "export" notification
		$scope.$broadcast('exportStart', {
			url : '/api/v1/export',
			data : data
		});
	};

	// load data
	$scope.load = function () {
		$scope.categories = $resource('/api/v1/amazon/bestsellers/categories')
		.query( function (categories) {
			categories.forEach(function (category) {
				category.products = $resource('/api/v1/amazon/bestsellers/category', {url : category.url})
				.query(function (products) {
					products.forEach(function (product) {
						$resource('/api/v1/amazon/bestsellers/product', {url : product.url})
						.get(function(item) {
							product.title = item.title || product.title;
							product.sellers = item.sellers;
							product.sellers_url = item.url;
						});
					});
				});
			});
		});
	};
}]);

