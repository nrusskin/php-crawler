'use strict';

angular.module('crawler.Examples', [])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('home', {
		abstract : true,
		views: {
			'viewMenu' : {
				templateUrl  : '/app/partials/menu.html'
			}
		}
	})
	.state('samples', {
		parent : 'home',
		url: '/app',
		views: {
			'content@': {
				templateUrl : '/app/partials/home.html',
				controller : 'HomeCtrl'
			}
		}
	});
}])

.controller('HomeCtrl', ['$scope', function ($scope) {
	$scope.plugins = [
	{
		title : 'Amazon.com best sellers',
		path : 'amazon',
		source : 'http://www.amazon.com/Best-Sellers/zgbs',
		description : `Here you can discover the best in Amazon Best Sellers, and find the top 100 most popular Amazon.`
	},
	{
		title : 'alibaba.com products',
		path : 'alibaba',
		source : 'https://alibaba.com',
		description : `Alibaba.com product list`
	},
	{
		title : 'Extract states',
		path : 'privateproperty',
		source : 'http://www.privateproperty.com.ng',
		description : `Scrapper extract all the state, town and area information from the site http://www.privateproperty.com.ng and export to the CSV file.`
	},
	{
		title : 'Scrape popular events',
		path : 'residentadvisor',
		source : 'https://www.residentadvisor.net',
		description : `RA Events is the real-world platform for our editorial content. We program and produce events that reflect our open-ended coverage of electronic music and global outlook, spanning arts spaces in London to nightclubs in Tokyo and most things in between.`
	},
	{
		title : 'Market crawler',
		path : 'matask',
		source : 'https://www.matas.dk',
		description : `Online e-commerce solution. The mission of Matas is “We want to help our customers feel good, look good and be in a good mood – at prices they can afford.”`
	},
	{
		title : 'ETL',
		path : 'lamudi',
		source : '/app/data/lamudi.com.ng.json',
		description : `Extract, Transform and Load JSON data to CSV file.`
	},
	{
		title : 'Email crawler',
		path : 'msci',
		source : 'http://www.msci.org/chapters',
		description : `Collect emails from the website`
	},
	{
		title : 'Links crawler',
		path : 'link',
		source : '',
		description : `Build web-site links list`
	}
	];

}]);
