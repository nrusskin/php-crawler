'use strict';

angular.module('crawler.Link', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('link', {
		parent: 'home',
		url: '/app/link',
		views: {
			'content@': {
				templateUrl : '/app/partials/link.html',
				controller : 'LinkCtrl'
			}
		}
	});

}])

.controller('LinkCtrl', ['$scope', '$resource', function ($scope, $resource) {
	var baseURI = '/api/v1/link/';
	$scope.url = 'http://google.com';

	$scope.search = function(url){
		$scope.items = [];
		$resource(baseURI, {url : url})
		.query( function (items) {
			$scope.inProgress = true;
			$scope.items = $scope.items.concat(items);
		}).$promise.finally(function(){
			$scope.inProgress = false;
		});
	}
}]);

