'use strict';

angular.module('crawler.Trova', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('trova', {
		url: '/app/trova',
		views: {
			'content@': {
				templateUrl : '/app/partials/trovabusinessdirectory.html',
				controller : 'TrovaCtrl'
			}
		}
	});

}])

.controller('TrovaCtrl', ['$scope', '$resource', function ($scope, $resource) {
	var baseURI = '/api/v1/trova/';
	$scope.items = [];

	for(var page = 1; page < 50; ++page)
	{
		$resource(baseURI + 'page/:page', {page : page})
		.query( function (items) {
			$scope.items = $scope.items.concat(items);
		});
	}
}]);

