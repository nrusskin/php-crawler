'use strict';

angular.module('crawler.Msci', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('msci', {
		parent: 'home',
		url: '/app/msci',
		views: {
			'content@': {
				templateUrl : '/app/partials/msci.html',
				controller : 'MsciCtrl'
			}
		}
	});

}])

.controller('MsciCtrl', ['$scope', '$resource', function ($scope, $resource) {
	var baseURI = '/api/v1/msci/chapt/:name';
	var chapts = [
		'michigan',
		'northohio', 
		'pittsburgh',
		'cincinnati',
		'indiana',
		'philadelphia',
		'centralstates'
	];

	$scope.items = [];
	chapts.forEach(function (chapt) {
		$resource(baseURI, {name : chapt})
		.query( function (items) {
			$scope.items = $scope.items.concat(items);
		});
	});

	$scope.export = function () {
		var data = $scope.items.slice();
		data.forEach(function (item) {
			delete item.$$hashKey;
		});
		$scope.$broadcast('exportStart', {
			url : '/api/v1/export',
			data : data
		});
	};
}]);

