'use strict';

angular.module('crawler.Matask', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('matask', {
		parent : 'home',
		url: '/app/matask',
		views: {
			'content@': {
				templateUrl : '/app/partials/matask.html',
				controller : 'MataskCtrl'
			}
		}
	});

}])

.controller('MataskCtrl', ['$scope', '$resource', function ($scope, $resource) {
	var baseURI = '/api/v1/matask/';
	$scope.items = [];

	for(var page = 1; page < 12; ++page)
	{
		$resource(baseURI + 'page/:page', {page : page})
		.query( function (items) {
			$scope.items = $scope.items.concat(items);
		});
	}
}]);

