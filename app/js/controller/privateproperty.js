'use strict';

angular.module('crawler.Privateproperty', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('privateproperty', {
		parent : 'home',
		url: '/app/privateproperty/',
		views: {
			'content@': {
				templateUrl: '/app/partials/privateproperty.html',
				controller: 'PrivatepropertyCtrl'
			}
		}
	});
}])

.controller('PrivatepropertyCtrl', ['$scope', '$resource', '$state', function ($scope, $resource, $state) {
	$scope.cities = [];
	$scope.load = function () {
		$scope.inProgress = true;
		$resource('/api/v1/privateproperty').query(
			function (data) {
				data.forEach(function (city) {
					city.towns.forEach( function (town) {
						town.suburbs.forEach( function (urbsarray) {
							var urbs = urbsarray.name.split('|');
							urbs.forEach( function(urb) {
								$scope.cities.push({
									'name' : city.name,
									'town' : town.name,
									'urb' : urb
								});
							});
						});
					});
				});
			},
			function (error) {

			}
		);
	};

	$scope.export = function() {
		var data = $scope.cities.slice();
		data.forEach(function (item) {
			delete item.$$hashKey;
		});
		$scope.$broadcast('exportStart', {
			url : '/api/v1/export',
			data : data
		});
	};
}]);

