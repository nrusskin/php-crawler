'use strict';

angular.module('crawler.Lamudi', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('lamudi', {
		parent : 'home',
		url: '/app/lamudi/',
		views: {
			'content@': {
				templateUrl: '/app/partials/lamudi.html',
				controller: 'LamudiCtrl'
			}
		}
	});
}])

.controller('LamudiCtrl', ['$scope', '$resource', function ($scope, $resource) {
	var regions = [];

	$scope.load = function () {
		$scope.inProgress = true;
		$resource('/app/data/lamudi.com.ng.json', {}, {query : {method : 'GET', isArray : true}})
		.query(function (data) {
			data.forEach(function (item) {
				if (item.area && item.area.items) {
					item.area.items.forEach(function (state) {
						regions.push({
							name : state.location_region_name,
							city : state.location_city_name,
							area : state.location_name
						});
					});
				}
			})
			var unique = {};
			regions.forEach(function(item) {
				unique[item.name + '|' + item.city + '|' + item.area] = item;
			});
			$scope.regions = [];
			for ( var key in unique )
				$scope.regions.push(unique[key]);
		});
	};

	$scope.export = function() {
		var data = regions.slice();
		data.forEach(function (item) {
			delete item.$$hashKey;
		});
		$scope.$broadcast('exportStart', {
			url : '/api/v1/export',
			data : data
		});
	};

}]);

