'use strict';

angular.module('crawler.Residentadvisor', ['ngResource'])

.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
	.state('residentadvisor', {
		parent : 'home',
		url: '/app/residentadvisor/:city',
		views: {
			'content@': {
				templateUrl: '/app/partials/residentadvisor.html',
				controller: 'ResidentadvisorCtrl'
			}
		}
	});
}])

.controller('ResidentadvisorCtrl', ['$scope', '$resource', '$stateParams', function ($scope, $resource, $stateParams) {
	$scope.events = [];
	
	if ($stateParams.city) {
		$scope.inProgress = true;
		$resource('/api/v1/residentadvisor/event')
		.query({
			selector : '.event-item > a',
			city : $stateParams.city
		},
		function (data) {
			data.forEach( function(eventId) {
				$resource('/api/v1/residentadvisor/event/:id', {id:eventId})
				.get({},
				function (event) {
					$scope.events.push(event);
				});
			});
		})
		.$promise
		.finally(function(){
			$scope.inProgress = false;
		});
	}
}]);

