<?php

require '../vendor/autoload.php';

$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
 
$export = new \PHPCrawler\Export($app, '/v1/export');
$amazon	= new \PHPCrawler\Amazon($app, '/v1/amazon');
$matask	= new \PHPCrawler\Matask($app, '/v1/matask');
$trova	= new \PHPCrawler\TrovaBusinessDirectory($app, '/v1/trova');
$link	= new \PHPCrawler\Link($app, '/v1/link');
$email = new \PHPCrawler\Msci($app, '/v1/msci');

$app->group('/v1', function () use ($app) {
	//https://angel.co/companies?locations[]=Singapore
	$app->group('/angel', function () use ($app) {
		$app->get('/startup', function () use ($app) {
			$client = new \Goutte\Client();
			$request = $client->getClient()->createRequest('GET','https://angel.co/companies/startups?' . $_SERVER['QUERY_STRING']);
			$request->setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.57 Safari/537.36');
			$request->setHeader('X-Requested-With', 'XMLHttpRequest');
			$response = $client->getClient()->send($request);
			$html = $response->getBody();

			#https://angel.co/golden-gate-ventures?utm_source=companies
			preg_match_all('/co\/([^\?|"]+)\?utm_source=companies/', $html, $url);
			$result = [];
			$uniq = array_unique($url[1]);
			foreach($uniq as $val)
				$result[] = $val;
			echo json_encode($result);
			return;

			$loader = new \PHPCrawler\Filter();
			$loader->setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.57 Safari/537.36'); 
			$loader->setHeader('X-Requested-With', 'XMLHttpRequest');
			$document = $loader->get('https://angel.co/companies/startups?' . $_SERVER['QUERY_STRING'], null, null);#'#startup-link', function ($link) {
#				echo $link;
#			});
			print_r($document->html());
		});

		$app->get('/startup/search/:city', function ($city) use ($app) {
			$client = new \Goutte\Client();
			$data = 'filter_data%5Blocations%5D%5B%5D=' . $city . '&sort=signal';
			$request = $client->getClient()->createRequest('POST','https://angel.co/company_filters/search_data', ['body' => $data]);
			$request->setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.57 Safari/537.36');
			$request->setHeader('X-Requested-With', 'XMLHttpRequest');
			$response = $client->getClient()->send($request);
			echo json_encode($response->json());
		});
		$app->get('/startup/:name', function ($name) use ($app) {
			$loader = new \PHPCrawler\Filter();
			$loader->setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.57 Safari/537.36'); 
			$document = $loader->get('https://angel.co/' . $name . '?utm_source=companies');	
			$result['name'] = $document->filter('.name_holder > h1')->text();
	
			$node = $document->filter('.company_url');
			if ($node)
				$result['website'] = $node->attr('href');

			$node = $document->filter('.twitter_url');
			if ($node)
				$result['twitter'] = $node->attr('href');

			$node = $document->filter('.facebook_url');
			if ($node)
				$result['facebook'] = $node->attr('href');

			$node = $document->filter('.linkedin_url');
			if ($node)
				$result['linkedin'] = $node->attr('href');

			$node = $document->filter('.windows > .content');
			if ($node)
				$result['description'] = $node->text();

			$result['users'] = [];
			$founders = $document->filter('.founders .role');
			if ($founders)
				$founders->each( function ($item) use (&$result) {
					$founder['name'] = $item->filter('.name')->text();
					$founder['title'] = $item->filter('.bio')->text();
					$result['users'][] = $founder;
				});

			$result['tag'] = [];
			$tags = $document->filter('.mobile .tags > .tag');
			if ($tags)
				$tags->each(function ($item) use (&$result) {
					$result['tag'][] = $item->text();
				});

			$node = $document->filter('.inner_section > .header > .type');
			if ($node)
				$result['stage'] = $node->text();

			$node = $document->filter('.raised');
			if ($node)
				$result['amount'] = $node->text();

			echo json_encode($result);
		});
	});

	//http://www.newchoicehealth.com/places/washington-dc/washington/ct-scan
	$app->group('/newchoicehealth', function () use ($app) {
		$app->get('/', function () use ($app) {
			$result = [];
			$loader = new \PHPCrawler\Filter();
			$crawler = $loader->get(
				'http://www.newchoicehealth.com/places/washington-dc/washington/arthroscopic-shoulder-surgery',
				#'http://www.newchoicehealth.com/places/washington-dc/washington/ct-scan',
				'#procedures-table > tbody > tr > td > a', 
				function ($node) use($loader, &$result) { 
					if (count($node->attr('class')) == 0) {
						$item['name'] = $node->text();
						$item['url'] = 'http://www.newchoicehealth.com' . $node->attr('href');
						$item['price'] = [];
						$loader->get('http://www.newchoicehealth.com' . $node->attr('href'), '.prices > div', function ($node) use (&$item) {
							$matches = [];
							preg_match('/([0-9,]+)/', $node->text(), $matches);
							$item['price'][] = $matches[0];
						});
						$result[] = $item;
					}
			});
			echo json_encode($result);
		});
	});

	//http://www.lamudi.com.ng/search/locationSuggest?searchString=a
	$app->group('/lamudi', function () use ($app) {
		$app->get('/:letter', function ($letter) use ($app) {
			$loader = new \PHPCrawler\Filter();
			$data = $loader->get('http://www.lamudi.com.ng/search/locationSuggest?searchString=' . $letter);
			print_r($data->html());
		});
	});

	//http://www.privateproperty.com.ng/
	$app->group('/privateproperty', function () use ($app) {
		$app->get('/', function () use ($app) {
			$loader = new \PHPCrawler\Filter();
			$loader->get('http://www.privateproperty.com.ng/', '#page-footer > #page > script', function ($node, $i) {
				if ($i == 1) {
					$script = $node->text();
					preg_match('/(var cascadedProvinces = )(.+\]);/', $script, $data);
					print_r($data[2]);;
				}
			});
			
		});
	});

	$app->group('/residentadvisor', function () use ($app) {
		/**
		 * @return Event list ID's
		 */
		$app->get('/event', function () use ($app) {
			$params = $app->request->get();

			if (!isset($params['city']) || !isset($params['selector']))
				$app->halt(400, 'Missing parameter "city" or "selector"');

			$selector = $params['selector'];
			$url = 'https://www.residentadvisor.net/events.aspx?' .
				'ai=' . $params['city'] . 
				'&v=day' .
				'&mn=' . date('m') . '&yr=' . date('Y') . '&dy=' . date('d');

			$result = [];
			$loader = new \PHPCrawler\Filter();
			$loader->get($url, $selector, function ($node) use (&$result) {
				$url = $node->attr('href');
				preg_match('/([0-9]+)$/', $url, $id);
				$result[] = $id[0];
			});

			echo json_encode($result);
		});

		/**
		 * @return Event information
		 */
		$app->get('/event/:id', function ($id) {
			$server = 'https://www.residentadvisor.net';
			$url = $server . '/event.aspx?' . $id;
			$result['url'] = $url;

			$loader = new \PHPCrawler\Filter();
			$event = $loader->get($url);
			
			$result['title'] = $event->filter('#sectionHead > h1')->first()->text();

			$info = $event->filter('.clearfix > li')->first();
			$result['date'] = $info->filter('a')->text();

			$html = $info->html();
			preg_match('/([^\>]+)$/', $html, $time);
			$result['time'] = $time[0];

			$info = $event->filter('.clearfix > .wide')->first();
			$html = $info->html();
			preg_match('/\<br\>([^\>]+)\<br\>/', $html, $address);
			if (count($address) > 1)
				$result['address'] = $address[1];

			$info = $event->filter('#event-item > div > p')->eq(1);
			$result['description'] = $info->text();

			$info = $event->filter('#event-item > div > a > img');
			if ($info->count() > 0)
				$result['img'] = $server . $info->attr('src');

			echo json_encode($result);			
		});
	});
 });

$app->run();
